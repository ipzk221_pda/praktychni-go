package main

import (
	"fmt"
	"math"
)

func main() {
	var a = 2147483629
	var c = 2147483587
	var m = int(math.Pow(2, 31)) - 1
	var seed = 21
	for i := 0; i < 40000; i++ {
		seed = ((a * seed) + c) % m
		fmt.Println(seed)
	}
}
