// main
package main

import (
	mymath "ex3/math"
	"fmt"
)

func main() {
	sum := mymath.Add(1.3, 2.33, -3.2)
	fmt.Println(sum)
}
