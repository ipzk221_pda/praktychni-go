package main

import "fmt"

func main() {
	var first, second bool
	var third bool = true
	fourth := !third
	var fifth = true

	fmt.Println("first  = ", first)       // false
	fmt.Println("second = ", second)      // false
	fmt.Println("third  = ", third)       // true
	fmt.Println("fourth = ", fourth)      // false
	fmt.Println("fifth  = ", fifth, "\n") // true

	fmt.Println("!true  = ", !true)        // false
	fmt.Println("!false = ", !false, "\n") // true

	fmt.Println("true && true   = ", true && true)         // true
	fmt.Println("true && false  = ", true && false)        // false
	fmt.Println("false && false = ", false && false, "\n") // false

	fmt.Println("true || true   = ", true || true)         // true
	fmt.Println("true || false  = ", true || false)        // true
	fmt.Println("false || false = ", false || false, "\n") // false

	fmt.Println("2 < 3  = ", 2 < 3)        // true
	fmt.Println("2 > 3  = ", 2 > 3)        // false
	fmt.Println("3 < 3  = ", 3 < 3)        // false
	fmt.Println("3 <= 3 = ", 3 <= 3)       // true
	fmt.Println("3 > 3  = ", 3 > 3)        // false
	fmt.Println("3 >= 3 = ", 3 >= 3)       // true
	fmt.Println("2 == 3 = ", 2 == 3)       // false
	fmt.Println("3 == 3 = ", 3 == 3)       // true
	fmt.Println("2 != 3 = ", 2 != 3)       // true
	fmt.Println("3 != 3 = ", 3 != 3, "\n") // false

	//Задание.
	//1. Пояснить результаты операций
	/*
		1) first, second не ініціолізовано, за  тому замовчуванням стоїть нуль
		third, fifth ініціолізовано true
		fourth - стоїть знак заперечення
		2) !true, !false - стоїть логічна операція "НЕ"
		3) логічна операція "І" (множення) (1*1=1; 1*0=0; 0*1=0)
		4) логічна операція "АБО" (додавання) (1+1=1; 1+0=1; 0+1=1)
		5) Якщо вираз задовольняє умову,результат буде 1 (істина).
		=============================================
		== - дорівнює
		!= - не дорівнює
		<= - менше або дорівнює
		>= - більше аюо дорівнює
		< - менше
		> - більше

	*/
}
