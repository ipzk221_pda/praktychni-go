package main

import "fmt"

func main() {
	//Инициализация переменных
	var userinit8 uint8 = 1
	var userinit16 uint16 = 2
	var userinit64 int64 = -3
	var userautoinit = -4 //Такой вариант инициализации также возможен

	fmt.Println("Values: ", userinit8, userinit16, userinit64, userautoinit, "\n")

	//Краткая запись объявления переменной
	//только для новых переменных
	intVar := 10

	fmt.Printf("intVar = "+"Type = %T Value = %d\n", intVar, intVar)

	//Задание.
	//1. Вывести типы всех переменных
	fmt.Printf("Задання 1\n")
	fmt.Printf("userinit8 = "+"Type = %T Value = %d\n", userinit8, userinit8)
	fmt.Printf("userinit16 = "+"Type = %T Value = %d\n", userinit16, userinit16)
	fmt.Printf("userinit64 = "+"Type = %T Value = %d\n", userinit64, userinit64)
	fmt.Printf("userautoinit = "+"Type = %T Value = %d\n", userautoinit, userautoinit)

	//2. Присвоить переменной intVar переменные userinit16 и userautoinit. Результат вывести.
	intVar = int(userautoinit)
	fmt.Printf("intVar = "+"Type = %T Value = %d\n", intVar, intVar)
	intVar = int(userinit16)
	fmt.Printf("intVar = "+"Type = %T Value = %d\n", intVar, intVar)

}
